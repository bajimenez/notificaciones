//
//  ViewController.swift
//  notificaciones
//
//  Created by Brayan Jimenez on 10/1/18.
//  Copyright © 2018 Brayan Jimenez. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    @IBOutlet weak var ingresoTextField: UITextField!
    @IBOutlet weak var resultadoLabel: UILabel!
    
    var notificationMessage = "El texto es: "
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Para recuperar
        //1. Instanciar UserDefaults
        let defaults = UserDefaults.standard
        
        //2. Acceder al valor por medio del KEY
        
        resultadoLabel.text = defaults.object(forKey: "label") as? String
        
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]){(granted, error) in
            
        }
        
        
    }

   
    @IBAction func guardarButton(_ sender: Any) {
        resultadoLabel.text = ingresoTextField.text
        //Para guardar
        //1. Instanciar UserDefaults
        let defaults = UserDefaults.standard
        
        //2. Guardar variables en defaults
        //int, Bool, Float, Double, String, Object!
        
        defaults.set(ingresoTextField.text, forKey: "label")
        
        notificationMessage += resultadoLabel.text!
        sendNotification()
    }
    
    func sendNotification() {
        
        notificationMessage += resultadoLabel.text!
        
        //1. Authorization request (esta en DidLoad)
        // 2. Crear contenido de la notificacion
        
        let content = UNMutableNotificationContent()
        content.title = "Notification Tittle"
        content.subtitle = "Notification Subtittle"
        content.body = notificationMessage
        
        //2.1 crear acciones
        
        let repeatActions = UNNotificationAction(identifier: "repeat", title:"repetir", options:[])
        let changeMessage = UNTextInputNotificationAction(identifier: "change", title: "cambiar mensaje", options: [])
        
        //2.2 agregar acciones a un UNNotiiactionCategory
        let category = UNNotificationCategory(identifier: "actionsCat", actions: [repeatActions, changeMessage], intentIdentifiers: [], options: [])
        //2.3 agregar el categoryIdentifier al content
        
        content.categoryIdentifier = "actionsCat"
        
        //2.4 agregar category al UNUsernotificationCenter
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        //3. Definir un trigger
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        //4. Definir un identifier para la notificacion
        
        let identifier = "Notificacion"
        
        //5. Crear un Request
        
        let notificationRequest = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        //6. añadir el request al UNUserNotificationCenter
        
        UNUserNotificationCenter.current().add(notificationRequest){ (error) in
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch  response.actionIdentifier {
        case "repeat":
            self.sendNotification()
            break
        case "change":
            let txtResponse = response as! UNTextInputNotificationResponse
            notificationMessage += txtResponse.userText
            self.sendNotification()
            completionHandler()
            break
        default:
            break
        }
     print(response.actionIdentifier)
        completionHandler()
        
    }

}


